require 'socket'

class PasswordController < ApplicationController

  def generate
    password = { password: { value: SecureRandom.urlsafe_base64, server: Socket.gethostname } }
    render json: password
  end

end