Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/api' do
    scope '/v1' do
      resource :password, only: [] do
        member do
          get '/generate' => 'password#generate'
        end
      end
    end
  end
end
